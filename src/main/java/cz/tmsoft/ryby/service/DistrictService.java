package cz.tmsoft.ryby.service;

import cz.tmsoft.ryby.domain.District;
import cz.tmsoft.ryby.repository.DistrictRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author tomas.marianek
 */
@Service
public class DistrictService {
    private DistrictRepository districtRepository;

    public DistrictService(DistrictRepository districtRepository) {
        this.districtRepository = districtRepository;
    }

    public List<District> getAllDistrict() {
        return districtRepository.getAllDistrict();
    }

    public List<District> getAllDistrict(String number) {
        return districtRepository.getAllDistrictByDistrict(number);
    }
}
