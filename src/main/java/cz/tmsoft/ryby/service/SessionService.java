package cz.tmsoft.ryby.service;

import cz.tmsoft.ryby.domain.Pass;
import cz.tmsoft.ryby.domain.Session;
import cz.tmsoft.ryby.domain.User;
import cz.tmsoft.ryby.exceptions.SessionNotFoundException;
import cz.tmsoft.ryby.exceptions.UserNotFoundException;
import cz.tmsoft.ryby.exceptions.UserPasswordDoNotMatchException;
import cz.tmsoft.ryby.repository.SessionRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * @author tomas.marianek
 */
@Service
@Transactional
public class SessionService {

    private SessionRepository sessionRepository;

    public SessionService(SessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }


    /**
     * nastavi aktualni token na nepklatny...valid_to na aktialni cas
     * @param sessionUuid
     */
    public void logout(String sessionUuid){
        sessionRepository.removeSessionToken(sessionUuid);
    }

    /**
     * 1. dohleda uzivate => ok or not ok -> exception
     * 2. dohleda vsechny validni session pro uzivate => 0 ,ok -> vsechny znevalidni
     * 3. zalozi session pro uzivatele s delkou platnosti 1 hodinu
     * 4. vrati session uuid jako totek
     * @param user
     * @return
     */
    public String login(User user) {
        User dbUser = sessionRepository.getUsersByEmail(user.getName());
        if(dbUser == null){
            throw new UserNotFoundException();
        }

        Pass pass = sessionRepository.getUserPassword(dbUser.getId(), LocalDateTime.now());
        if(pass == null || !pass.equals(user.getPass())){
            throw  new UserPasswordDoNotMatchException();
        }

        sessionRepository.deleteAllSessionForUser(dbUser.getId());
        Session session = Session.builder()
                .idUsers(dbUser.getId())
                .sessionUuid(UUID.randomUUID().toString())
                .validFrom(LocalDateTime.now())
                .validTo(LocalDateTime.now().plusMinutes(10))
                .build();
        sessionRepository.createNewSession(session);
        return session.getSessionUuid();
    }

    /**
     * prodlouzi platnost tokenu o 10 minut
     * @param sessionToken
     */
    public void renewToken(String sessionToken) {
        Session session = sessionRepository.getSessionToken(sessionToken, LocalDateTime.now());
        if(session == null){
            throw new SessionNotFoundException();
        }
        session.setValidTo( LocalDateTime.now().plusMinutes(10));
        sessionRepository.updateSessionTokenValidTo(session);
    }

    public Long getUserIdForSession(String sessionToken) {
        return sessionRepository.getUserIdForSession(sessionToken);
    }
}
