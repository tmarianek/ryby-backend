package cz.tmsoft.ryby.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

import cz.tmsoft.ryby.domain.AttendaceClose;
import cz.tmsoft.ryby.domain.Permit;
import cz.tmsoft.ryby.domain.enums.PermitFish;
import cz.tmsoft.ryby.repository.AttendanceRepository;
import cz.tmsoft.ryby.repository.PermitRepository;

/**
 * @author tomas.marianek
 */
@Service
@Transactional
public class PermitSevice {

    private PermitRepository permitRepository;
    private SessionService sessionService;

    private AttendanceRepository attendanceRepository;


    public PermitSevice(PermitRepository permitRepository,
                        SessionService sessionService,
                        AttendanceRepository attendanceRepository) {
        this.permitRepository = permitRepository;
        this.sessionService = sessionService;
        this.attendanceRepository = attendanceRepository;
    }

    public List<Permit> getAllPermit(String sessionToken) {
        return permitRepository.getAllPermit(sessionToken);
    }

    public Permit getPermit(String uuid) {
        return permitRepository.getPermit(uuid);
    }

    public void savePermit(Permit permit, String sessionToken) {
        Long userId = sessionService.getUserIdForSession(sessionToken);
        permit.setUserId(userId);
        permit.setUuid(UUID.randomUUID().toString());
        permit.setStav("a");
        permitRepository.savePermit(permit);
    }

    public void deletePermit(String uuid) {
        permitRepository.deletePermit(uuid);
    }

    public void updatePermit(Permit permit) {
        permitRepository.updatePermition(permit);
    }

    public List<AttendaceClose> getClosePermit(String uuid) {
        List<AttendaceClose> closeAttendance = attendanceRepository.getCloseAttendance(uuid);

        for (AttendaceClose close : closeAttendance) {
            close.setKapr(
                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.KAPR_OBECNY.getValue())));
            close.setLin(
                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.LIN_OBECNY.getValue())));
            close.setCejn(
                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), PermitFish.getCejn()));
            close.setAmur(
                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.AMUR_BILY.getValue())));
            close.setCandat(attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), PermitFish.getCandat()));
            close.setBolen(
                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.BOLEN_DRAVY.getValue())));
            close.setHlavatka(
                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.HLAVATKA_OBECNA.getValue())));
            close.setKaras(attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), PermitFish.getKaras()));
            close.setLipan(
                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.LIPAN_PODHORNI.getValue())));
            close.setMnik(
                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.MNIK_JEDNOVOUSY.getValue())));
            close.setParma(
                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.PARMA_OBECNA.getValue())));
            close.setJelecJesen(
                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.JELEC_JESEN.getValue())));
            close.setTloust(
                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.JELEC_TLOUST.getValue())));
            close.setOkoun(
                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), PermitFish.getOkoun()));
            close.setOstroretka(attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(),
                                                                                List.of(PermitFish.OSTRORETKA_STEHOVAVA.getValue())));
            close.setPodoustev(
                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.PODOUSTEV_RICNI.getValue())));
            close.setPstruhDuhovy(
                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.PSTRUH_DUHOVY.getValue())));
            close.setPstruhObecny(
                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.PSTRUH_OBECNY.getValue())));
            close.setSiven(
                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.SIVEN_AMERICKY.getValue())));
            close.setMarenaPeled(attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), PermitFish.getSin()));
            close.setStika(
                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.STIKA_OBECNA.getValue())));
            close.setSumec(
                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.SUMEC_VELKY.getValue())));
            close.setTolstolobik(
                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.TOLSTOLOBIK_BILY.getValue())));
            close.setUhor(attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.UHOR_RICNI.getValue())));
            close.setOstatni(attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), PermitFish.getOstatni()));
        }

        return closeAttendance;
    }
}
