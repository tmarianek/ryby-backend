package cz.tmsoft.ryby.service;

import cz.tmsoft.ryby.domain.Registry;
import cz.tmsoft.ryby.repository.RegistryRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author tomas.marianek
 */
@Service
@Transactional
public class RegistryService {

    private RegistryRepository registryRepository;

    public RegistryService(RegistryRepository registryRepository) {
        this.registryRepository = registryRepository;
    }

    public List<Registry> getRegistryByCode(String code) {
        return registryRepository.getRegistryByCode(code);
    }
}
