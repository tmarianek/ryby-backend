package cz.tmsoft.ryby.service;

import cz.tmsoft.ryby.domain.Attendance;
import cz.tmsoft.ryby.domain.Permit;
import cz.tmsoft.ryby.repository.AttendanceRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

/**
 * @author tomas.marianek
 */
@Service
@Transactional
public class AttendanceService {
    private AttendanceRepository attendanceRepository;
    private PermitSevice permitSevice;

    public AttendanceService(AttendanceRepository attendanceRepository, PermitSevice permitSevice) {
        this.attendanceRepository = attendanceRepository;
        this.permitSevice = permitSevice;
    }

    public List<Attendance> getAttendanceByPermission(String uuid) {
        return attendanceRepository.getAttendanceByPermission(uuid);
    }

    public void saveAttendance(String uuid, Attendance attendance) {
        Permit permit = permitSevice.getPermit(uuid);
        attendance.setIdPovolenka(permit.getId());
        attendance.setUuid(attendance.getUuid() == null ? UUID.randomUUID().toString() : attendance.getUuid());
        attendanceRepository.saveAttendance(attendance);
    }
}
