package cz.tmsoft.ryby.service;

import cz.tmsoft.ryby.domain.Pass;
import cz.tmsoft.ryby.domain.Registration;
import cz.tmsoft.ryby.domain.User;
import cz.tmsoft.ryby.exceptions.UserAlreadyExistException;
import cz.tmsoft.ryby.repository.PasswordRepository;
import cz.tmsoft.ryby.repository.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * @author tomas.marianek
 */
@Service
@Transactional
public class RegistrationService {
    private UserRepository userRepository;
    private PasswordRepository passwordRepository;

    public RegistrationService(UserRepository userRepository, PasswordRepository passwordRepository) {
        this.userRepository = userRepository;
        this.passwordRepository = passwordRepository;
    }

    public void registration(Registration registration) {
        User dbUser = userRepository.getUserByEmail(registration.getEmail());
        if(dbUser != null){
            throw  new UserAlreadyExistException();
        }
        User user = new User();
        user.setName(registration.getName());
        user.setEmail(registration.getEmail());
        user.setUuid(UUID.randomUUID().toString());
        userRepository.createUser(user);

        Pass pass = new Pass();
        pass.setPass(registration.getPassword());
        pass.setIdUser(user.getId());
        pass.setValidFrom(LocalDateTime.now());
        pass.setValidTo(LocalDateTime.now().plusDays(30));
        passwordRepository.createPassword(pass);
    }
}
