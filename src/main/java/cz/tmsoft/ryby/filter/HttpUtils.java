package cz.tmsoft.ryby.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import cz.tmsoft.ryby.errors.ErrorInfo;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;

/**
 * @author tomas.marianek
 */
public class HttpUtils {

    public static final ObjectMapper objectMapper = new ObjectMapper();

    static {
        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ"));
        objectMapper.registerModule(new JavaTimeModule());
    }

    public static void setStatusCodeUnauthorized(HttpServletResponse response, HttpServletRequest request) throws IOException {
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);

        ErrorInfo errorInfo = ErrorInfo.builder()
                .errorCode(HttpStatus.UNAUTHORIZED.toString())
                .httpStatus(HttpStatus.UNAUTHORIZED)
                .url(request.getRequestURI())
                .timeStamp(LocalDateTime.now())
                .description("Missing session_token")
                .build();
        response.getWriter().write(objectMapper.writeValueAsString(errorInfo));
    }

    public static void setSessionNotFound(HttpServletResponse response, HttpServletRequest request) throws IOException {
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);

        ErrorInfo errorInfo = ErrorInfo.builder()
                .errorCode(HttpStatus.UNAUTHORIZED.toString())
                .httpStatus(HttpStatus.UNAUTHORIZED)
                .url(request.getRequestURI())
                .timeStamp(LocalDateTime.now())
                .description("Session not found")
                .build();
        response.getWriter().write(objectMapper.writeValueAsString(errorInfo));
    }
}
