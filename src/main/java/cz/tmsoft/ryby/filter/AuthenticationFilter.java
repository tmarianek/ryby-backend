package cz.tmsoft.ryby.filter;

import cz.tmsoft.ryby.exceptions.SessionNotFoundException;
import cz.tmsoft.ryby.service.SessionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author tomas.marianek
 */
@Component
@Slf4j
public class AuthenticationFilter extends OncePerRequestFilter{

    @Autowired
    private SessionService sessionService;


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (!request.getRequestURI().contains("login") &&
                !request.getRequestURI().contains("swagger") &&
                !request.getRequestURI().contains("api-docs") &&
                !request.getRequestURI().contains("registration") &&
                !request.getRequestURI().contains("h2-console") &&
                !request.getRequestURI().contains("actuator") &&
            !request.getRequestURI().contains("favicon.ico")) {
            String sessionToken = request.getHeader("session_token");
            log.info("Filter Session_token: " + sessionToken);
            if (sessionToken == null) {
                HttpUtils.setStatusCodeUnauthorized(response, request);
                return;
            } else {
                try {
                    sessionService.renewToken(sessionToken);
                } catch (SessionNotFoundException ex) {
                    HttpUtils.setSessionNotFound(response, request);
                    return;
                }
            }
        }
        filterChain.doFilter(request, response);
    }

}
