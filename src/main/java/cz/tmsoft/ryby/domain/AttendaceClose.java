package cz.tmsoft.ryby.domain;

import java.io.Serial;
import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Data;

/**
 * @author tomas.marianek Since 30.09.2022
 */
@Data
public class AttendaceClose implements Serializable {
    @Serial
    private static final long serialVersionUID = -420205034599419158L;
    private String revir;
    private String podrevir;
    private String name;
    private Long count;

    private AttendanceCloseWithFist candat;
    private AttendanceCloseWithFist amur;
    private AttendanceCloseWithFist bolen;
    private AttendanceCloseWithFist cejn;
    private AttendanceCloseWithFist hlavatka;
    private AttendanceCloseWithFist jelecJesen;
    private AttendanceCloseWithFist tloust;
    private AttendanceCloseWithFist kapr;
    private AttendanceCloseWithFist karas;
    private AttendanceCloseWithFist lipan;
    private AttendanceCloseWithFist lin;
    private AttendanceCloseWithFist mnik;
    private AttendanceCloseWithFist okoun;
    private AttendanceCloseWithFist ostroretka;
    private AttendanceCloseWithFist parma;
    private AttendanceCloseWithFist podoustev;
    private AttendanceCloseWithFist pstruhDuhovy;
    private AttendanceCloseWithFist pstruhObecny;
    private AttendanceCloseWithFist siven;
    private AttendanceCloseWithFist sumec;
    private AttendanceCloseWithFist marenaPeled;
    private AttendanceCloseWithFist stika;
    private AttendanceCloseWithFist tolstolobik;
    private AttendanceCloseWithFist uhor;
    private AttendanceCloseWithFist ostatni;


    public Long getSumAll() {
        Long sum = 0L;
        sum += candat.getPocet();
        sum += amur.getPocet();
        sum += bolen.getPocet();
        sum += cejn.getPocet();
        sum += hlavatka.getPocet();
        sum += jelecJesen.getPocet();
        sum += tloust.getPocet();
        sum += kapr.getPocet();
        sum += karas.getPocet();
        sum += lipan.getPocet();
        sum += lin.getPocet();
        sum += mnik.getPocet();
        sum += okoun.getPocet();
        sum += ostroretka.getPocet();
        sum += parma.getPocet();
        sum += podoustev.getPocet();
        sum += pstruhDuhovy.getPocet();
        sum += pstruhObecny.getPocet();
        sum += siven.getPocet();
        sum += sumec.getPocet();
        sum += marenaPeled.getPocet();
        sum += stika.getPocet();
        sum += tolstolobik.getPocet();
        sum += uhor.getPocet();
        sum += ostatni.getPocet();

        return sum;
    }

    public BigDecimal getSumAllWeight() {
        BigDecimal sum = BigDecimal.ZERO;
        sum = sum.add(candat.getSum());
        sum = sum.add(amur.getSum());
        sum = sum.add(bolen.getSum());
        sum = sum.add(cejn.getSum());
        sum = sum.add(hlavatka.getSum());
        sum = sum.add(jelecJesen.getSum());
        sum = sum.add(tloust.getSum());
        sum = sum.add(kapr.getSum());
        sum = sum.add(karas.getSum());
        sum = sum.add(lipan.getSum());
        sum = sum.add(lin.getSum());
        sum = sum.add(mnik.getSum());
        sum = sum.add(okoun.getSum());
        sum = sum.add(ostroretka.getSum());
        sum = sum.add(parma.getSum());
        sum = sum.add(podoustev.getSum());
        sum = sum.add(pstruhDuhovy.getSum());
        sum = sum.add(pstruhObecny.getSum());
        sum = sum.add(siven.getSum());
        sum = sum.add(sumec.getSum());
        sum = sum.add(marenaPeled.getSum());
        sum = sum.add(stika.getSum());
        sum = sum.add(tolstolobik.getSum());
        sum = sum.add(uhor.getSum());
        sum = sum.add(ostatni.getSum());

        return sum;
    }
}
