package cz.tmsoft.ryby.domain;

import lombok.Data;

/**
 * @author tomas.marianek
 */
@Data
public class Registry {

    private String code;
    private String name;
}
