package cz.tmsoft.ryby.domain;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * @author tomas.marianek
 */
@Data
public class Attendance {
    private Long id;
    private Long idPovolenka;
    private String uuid;
    private LocalDate datum;
    private String revir;
    private String podrevir;
    private String revirName;
    private String podrevirName;
    private String druh;
    private String mnozstvi;
    private BigDecimal vaha;
    private String delka;
}
