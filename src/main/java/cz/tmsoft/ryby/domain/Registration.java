package cz.tmsoft.ryby.domain;

import lombok.Data;

/**
 * @author tomas.marianek
 */
@Data
public class Registration {
    private String name;
    private String email;
    private String password;
}
