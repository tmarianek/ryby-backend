package cz.tmsoft.ryby.domain;

import java.math.BigDecimal;

import lombok.Data;

/**
 * @author tomas.marianek Since 30.09.2022
 */
@Data
public class AttendanceCloseWithFist {
    private String name;
    private String code;
    private Long pocet;
    private BigDecimal sum;

}
