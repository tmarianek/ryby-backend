package cz.tmsoft.ryby.domain;

import lombok.Data;

/**
 * @author tomas.marianek
 */
@Data
public class District {
    private String number;
    private String name;
}
