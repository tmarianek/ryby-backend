package cz.tmsoft.ryby.domain;

import java.time.LocalDateTime;

import lombok.Data;

/**
 * @author tomas.marianek
 */
@Data
public class Pass {
    private long id;
    private Long idUser;
    private String pass;
    private LocalDateTime validFrom;
    private LocalDateTime validTo;
}
