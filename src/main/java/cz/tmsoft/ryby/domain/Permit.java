package cz.tmsoft.ryby.domain;

import lombok.Data;

import java.time.LocalDate;

/**
 * @author tomas.marianek
 */
@Data
public class Permit {

    private Long id;
    private Long userId;
    private String uuid;
    private String name;
    private String svaz;
    private String svazCode;
    private String druh;
    private String druhCode;
    private String kategorie;
    private String kategorieCode;
    private String typ;
    private String typCode;
    private String mistniOrganizace;
    private LocalDate platnostOd;
    private LocalDate platnostDo;
    private String stav;
    private String stavCode;

}
