package cz.tmsoft.ryby.domain.enums;

import java.util.List;

/**
 * @author tomas.marianek Since 29.09.2022
 */
public enum PermitFish {
    AMUR_BILY("a"),
    BOLEN_DRAVY("b"),
    CANDAT_OBECNY("c"),
    CANDAT_VYCHODNI("d"),
    CEJN_PERLETOVY("e"),
    CEJN_SINY("f"),
    CEJN_VELKY("g"),
    CEJNEK_MALY("h"),
    DRSEK_MENSI("j"),
    DRSEK_VETSI("k"),
    HLAVATKA_OBECNA("l"),
    HLAVAC_CERNOUSTY("m"),
    HLAVACKA_MRAMOROVANA("n"),
    HORAVKA_DUHOVA("o"),
    HROUZEK_KESSLERUV("p"),
    HROUZEK_BELOPLOUTVY("q"),
    HROUZEK_OBECNY("r"),
    JELEC_JESEN("s"),
    JELEC_PROUDNIK("t"),
    JELEC_TLOUST("u"),
    JESETER_HVEZDNATY("v"),
    JESETER_MALY("w"),
    JESETER_RUSKY("x"),
    JESETER_SIBIRSKY("y"),
    JESETER_VELKY("z"),
    JEZDIK_DUNAJSKY("aa"),
    JEZDIK_OBECNY("ab"),
    JEZDIK_ZLUTY("ac"),
    KAPR_OBECNY("ad"),
    KARAS_OBECNY("ae"),
    KARAS_STRIBRITY("af"),
    KOLJUSKA_TRIOSTNA("ag"),
    LIPAN_PODHORNI("ah"),
    LIN_OBECNY("ai"),
    LOSOS_OBECNY("aj"),
    MNIK_JEDNOVOUSY("ak"),
    MRENKA_MRAMOROVANA("al"),
    OKOUN_RICNI("am"),
    OKOUNEK_PSTRUHOVY("an"),
    OSTRORETKA_STEHOVAVA("ao"),
    OSTRUCHA_KRIVOCARA("ap"),
    OUKLEJ_OBECNA("aq"),
    OUKLEJKA_PRUHOVANA("ar"),
    PARMA_OBECNA("as"),
    PERLIN_OSTROBRICHY("at"),
    PISKOR_PRUHOVANY("au"),
    PLOTICE_LESKLA("av"),
    PLOTICE_OBECNA("aw"),
    PODOUSTEV_RICNI("ax"),
    PSTRUH_DUHOVY("ay"),
    PSTRUH_OBECNY("az"),
    SEKAVCIK_HORSKY("ba"),
    SEKAVEC_PODUNAJSKY("bb"),
    SIVEN_AMERICKY("bc"),
    SLUNECNICE_PESTRA("bd"),
    SLUNKA_OBECNA("be"),
    STREVLE_POTOCNI("bf"),
    STREVLICKA_VYCHODNI("bg"),
    SUMEC_VELKY("bh"),
    SIH_MARENA("bi"),
    SIH_PELED("bj"),
    SUMECEK_AMERICKY("bk"),
    SUMECEK_TECKOVANY("bl"),
    STIKA_OBECNA("bm"),
    TOLSTOLOBEC_PESTRY("bn"),
    TOLSTOLOBIK_BILY("bo"),
    VRANKA_OBECNA("bp"),
    VRANKA_PRUHOPLOUTVA("bq"),
    VYZA_VELKA("br"),
    UHOR_RICNI("bs"),
    ;


    private final String value;

    PermitFish(String value) {
        this.value = value;
    }

    public static List<String> getOstatni() {
        return List.of(DRSEK_MENSI.getValue(),
                       DRSEK_VETSI.getValue(),
                       HLAVAC_CERNOUSTY.getValue(),
                       HLAVACKA_MRAMOROVANA.getValue(),
                       HORAVKA_DUHOVA.getValue(),
                       HROUZEK_KESSLERUV.getValue(),
                       HROUZEK_BELOPLOUTVY.getValue(),
                       HROUZEK_OBECNY.getValue(),
                       JELEC_PROUDNIK.getValue(),
                       JESETER_HVEZDNATY.getValue(),
                       JESETER_MALY.getValue(),
                       JESETER_RUSKY.getValue(),
                       JESETER_SIBIRSKY.getValue(),
                       JESETER_VELKY.getValue(),
                       JEZDIK_DUNAJSKY.getValue(),
                       JEZDIK_OBECNY.getValue(),
                       JEZDIK_ZLUTY.getValue(),
                       KOLJUSKA_TRIOSTNA.getValue(),
                       LOSOS_OBECNY.getValue(),
                       MRENKA_MRAMOROVANA.getValue(),
                       OSTRUCHA_KRIVOCARA.getValue(),
                       OUKLEJ_OBECNA.getValue(),
                       OUKLEJKA_PRUHOVANA.getValue(),
                       PERLIN_OSTROBRICHY.getValue(),
                       PISKOR_PRUHOVANY.getValue(),
                       PLOTICE_LESKLA.getValue(),
                       PLOTICE_OBECNA.getValue(),
                       SEKAVCIK_HORSKY.getValue(),
                       SEKAVEC_PODUNAJSKY.getValue(),
                       SLUNECNICE_PESTRA.getValue(),
                       SLUNKA_OBECNA.getValue(),
                       STREVLE_POTOCNI.getValue(),
                       STREVLICKA_VYCHODNI.getValue(),
                       SUMECEK_AMERICKY.getValue(),
                       SUMECEK_TECKOVANY.getValue(),
                       TOLSTOLOBEC_PESTRY.getValue(),
                       VRANKA_OBECNA.getValue(),
                       VRANKA_PRUHOPLOUTVA.getValue(),
                       VYZA_VELKA.getValue());
    }

    public String getValue() {
        return value;
    }

    public static List<String> getCejn() {
        return List.of(CEJN_PERLETOVY.getValue(),
                       CEJN_SINY.getValue(),
                       CEJN_VELKY.getValue(),
                       CEJNEK_MALY.getValue());
    }

    public static List<String> getCandat() {
        return List.of(CANDAT_VYCHODNI.getValue(),
                       CANDAT_OBECNY.getValue());
    }

    public static List<String> getKaras() {
        return List.of(KARAS_OBECNY.getValue(),
                       KARAS_STRIBRITY.getValue());
    }

    public static List<String> getOkoun() {
        return List.of(OKOUN_RICNI.getValue(),
                       OKOUNEK_PSTRUHOVY.getValue());
    }

    public static List<String> getSin() {
        return List.of(SIH_MARENA.getValue(),
                       SIH_PELED.getValue());
    }
}
