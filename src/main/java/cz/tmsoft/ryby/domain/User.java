package cz.tmsoft.ryby.domain;

import lombok.Data;

/**
 * @author tomas.marianek
 */
@Data
public class User {
    private Long id;
    private String uuid;
    private String email;
    private String name;
    private String pass;
}
