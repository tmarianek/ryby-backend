package cz.tmsoft.ryby.domain;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author tomas.marianek
 */
@Data
@Builder
public class Session {
    private Long id;
    private Long idUsers;
    private String sessionUuid;
    private LocalDateTime validFrom;
    private LocalDateTime validTo;
}
