package cz.tmsoft.ryby.domain.errors;

import org.springframework.http.HttpStatus;

/**
 * @author tomas.marianek
 */
public interface IErrorType {
    /**
     * HttpStatus from enum implementing this interface
     */
    HttpStatus getHttpStatus();

    /**
     * Enum.name() from enum implementing this interface
     */
    String getName();

    /**
     * Description from enum implementing this interface
     */
    String getDescription();
}
