package cz.tmsoft.ryby.domain.errors;

import org.springframework.http.HttpStatus;

/**
 * @author tomas.marianek
 */

public enum ErrorType implements IErrorType {

    INTERNAL_SERVER_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "Unexpected server error"),
    USER_NOT_FOUND(HttpStatus.NOT_FOUND, "User not found" ),
    PASSWORD_NOT_MATCH(HttpStatus.CONFLICT, "password do not match"),
    SESSION_NOT_FOUND(HttpStatus.UNAUTHORIZED, "Session not found" ),
    USER_ALREADY_EXIST(HttpStatus.CONFLICT, "User already exist");

    private final HttpStatus httpStatus;
    private final String description;

    ErrorType(HttpStatus httpStatus, String description) {
        this.httpStatus = httpStatus;
        this.description = description;
    }

    @Override
    public HttpStatus getHttpStatus() {
        return this.httpStatus;
    }

    @Override
    public String getName() {
        return name();
    }

    @Override
    public String getDescription() {
        return this.description;
    }


    public String getDisplayMessage() {
        return String.format("%s(%s)", name(), description);
    }

}
