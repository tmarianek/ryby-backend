package cz.tmsoft.ryby.domain.errors;

import java.io.Serial;
import java.io.Serializable;

import lombok.Data;

/**
 * @author tomas.marianek
 */

@Data
public class ErrorStatus implements Serializable {

    @Serial
    private static final long serialVersionUID = 1182392077048872145L;

    private String reason;

    public ErrorStatus(Exception e) {
        this.reason = e.getMessage();
    }
}
