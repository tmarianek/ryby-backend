package cz.tmsoft.ryby.exceptions;

import cz.tmsoft.ryby.domain.errors.ErrorType;
import cz.tmsoft.ryby.domain.errors.IErrorType;

/**
 * @author tomas.marianek
 */
public class UserNotFoundException extends AbstractBusinessLogicException{
    @Override
    public IErrorType getErrorType() {
        return ErrorType.USER_NOT_FOUND;
    }
}
