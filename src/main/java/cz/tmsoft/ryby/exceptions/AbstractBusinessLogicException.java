package cz.tmsoft.ryby.exceptions;

import cz.tmsoft.ryby.domain.errors.IErrorType;

/**
 * @author tomas.marianek
 */
public abstract class AbstractBusinessLogicException extends RuntimeException {

    public AbstractBusinessLogicException() {
        super();
    }

    public AbstractBusinessLogicException(String message) {
        super(message);
    }

    public abstract IErrorType getErrorType();
}

