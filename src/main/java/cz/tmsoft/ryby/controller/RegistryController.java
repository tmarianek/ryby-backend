package cz.tmsoft.ryby.controller;

import cz.tmsoft.ryby.api.RegistryRest;
import cz.tmsoft.ryby.domain.Registry;
import cz.tmsoft.ryby.mapper.RegistryMapper;
import cz.tmsoft.ryby.service.RegistryService;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author tomas.marianek
 */
@RestController
@RequestMapping("/api/v1/registry")
public class RegistryController {

    private RegistryService registryService;

    private RegistryMapper registryMapper;

    public RegistryController(RegistryService registryService, RegistryMapper registryMapper) {
        this.registryService = registryService;
        this.registryMapper = registryMapper;
    }

    @GetMapping("/{code}")
    @Parameter(in = ParameterIn.HEADER, description = "Session Token", name = "session_token", required = true, schema = @Schema(type = "string"))
    public List<RegistryRest> getRegistryByCode(@NotEmpty @PathVariable String code){
        List<Registry> retVal = registryService.getRegistryByCode(code);
        return registryMapper.map(retVal);
    }
}
