package cz.tmsoft.ryby.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tomas.marianek
 */
@RestController
@RequestMapping("/api/v1/test")
public class TestController {

    @GetMapping
    @Operation(summary = "test", description = "Test controller")
    @Parameter(in = ParameterIn.HEADER, description = "Session Token", name = "session_token", required = true, schema = @Schema(type = "string"))
    public String test() {
        return "OK";
    }
}
