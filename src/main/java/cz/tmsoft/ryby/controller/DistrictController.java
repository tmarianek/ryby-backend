package cz.tmsoft.ryby.controller;

import cz.tmsoft.ryby.api.DistrictRest;
import cz.tmsoft.ryby.mapper.DistrictMapper;
import cz.tmsoft.ryby.service.DistrictService;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author tomas.marianek
 */
@RestController
@RequestMapping("/api/v1/district")
public class DistrictController {

    private DistrictService districtService;

    private DistrictMapper districtMapper;


    public DistrictController(DistrictService districtService, DistrictMapper districtMapper) {
        this.districtService = districtService;
        this.districtMapper = districtMapper;
    }

    @GetMapping()
    @Parameter(in = ParameterIn.HEADER, description = "Session Token", name = "session_token", required = true, schema = @Schema(type = "string"))
    public List<DistrictRest> getAllDistrict() {
        return districtMapper.map(districtService.getAllDistrict());
    }

    @GetMapping("/{number}")
    @Parameter(in = ParameterIn.HEADER, description = "Session Token", name = "session_token", required = true, schema = @Schema(type = "string"))
    public List<DistrictRest> getAllDistrictByDistrict(@PathVariable String number) {
        return districtMapper.map(districtService.getAllDistrict(number));
    }

}
