package cz.tmsoft.ryby.controller;

import cz.tmsoft.ryby.api.RegistrationRest;
import cz.tmsoft.ryby.domain.Registration;
import cz.tmsoft.ryby.mapper.RegistrationMapper;
import cz.tmsoft.ryby.service.RegistrationService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.validation.Valid;


/**
 * @author tomas.marianek
 */
@RestController
@RequestMapping(value = "/api/v1/registration")
public class RegistrationController {

    private RegistrationService registrationService;

    private RegistrationMapper registrationMapper;

    public RegistrationController(RegistrationService registrationService, RegistrationMapper registrationMapper) {
        this.registrationService = registrationService;
        this.registrationMapper = registrationMapper;
    }

    @PostMapping()
    public void registration(@RequestBody @Valid RegistrationRest registrationRest) {
        Registration registration = registrationMapper.map(registrationRest);
        registrationService.registration(registration);
    }
}
