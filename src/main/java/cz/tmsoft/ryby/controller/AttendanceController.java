package cz.tmsoft.ryby.controller;

import cz.tmsoft.ryby.api.AttendanceRest;
import cz.tmsoft.ryby.mapper.AttendanceMapper;
import cz.tmsoft.ryby.service.AttendanceService;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;

import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;
import java.util.List;

/**
 * @author tomas.marianek
 */
@RestController
@RequestMapping("/api/v1/attendance")
public class AttendanceController {

    private AttendanceService attendanceService;

    private AttendanceMapper attendanceMapper;

    public AttendanceController(AttendanceService attendanceService, AttendanceMapper attendanceMapper) {
        this.attendanceService = attendanceService;
        this.attendanceMapper = attendanceMapper;
    }

    @GetMapping("/{uuid}")
    @Parameter(in = ParameterIn.HEADER, description = "Session Token", name = "session_token", required = true, schema = @Schema(type = "string"))
    public List<AttendanceRest> getAllAttendance(@PathVariable String uuid) {
        return attendanceMapper.map(attendanceService.getAttendanceByPermission(uuid));
    }

    @PostMapping("/{uuid}")
    @Parameter(in = ParameterIn.HEADER, description = "Session Token", name = "session_token", required = true, schema = @Schema(type = "string"))
    public void saveAttendance(@PathVariable String uuid, @RequestBody @Valid AttendanceRest request){
        attendanceService.saveAttendance(uuid,attendanceMapper.map(request));
    }



}
