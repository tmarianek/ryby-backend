package cz.tmsoft.ryby.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import cz.tmsoft.ryby.api.SessionTokenRest;
import cz.tmsoft.ryby.api.UserRest;
import cz.tmsoft.ryby.domain.User;
import cz.tmsoft.ryby.errors.ErrorInfo;
import cz.tmsoft.ryby.mapper.SessionMapper;
import cz.tmsoft.ryby.service.SessionService;

import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.validation.Valid;

/**
 * @author tomas.marianek
 */
@RestController
@RequestMapping("/api/v1/session")
public class SessionController {

    private SessionService sessionService;

    private SessionMapper sessionMapper;

    public SessionController(SessionService sessionService, SessionMapper sessionMapper) {
        this.sessionService = sessionService;
        this.sessionMapper = sessionMapper;
    }

    @DeleteMapping("/logout")
    @Parameter(in = ParameterIn.HEADER, description = "Session Token", name = "session_token", required = true, schema = @Schema(type = "string"))
    public void logout(@RequestHeader Map<String, String> headers) {
        String sessionToken = headers.get("session_token");
        sessionService.logout(sessionToken);
    }

    @PostMapping("/login")
    @ApiResponse(responseCode = "404", description = "User Not found", content = {
        @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorInfo.class))
    })
    @ApiResponse(responseCode = "409", description = "Password not match", content = {
        @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorInfo.class))
    })
    public SessionTokenRest login(@Valid @RequestBody UserRest userRest) {
        User user = sessionMapper.map(userRest);
        String token = sessionService.login(user);
        SessionTokenRest response = new SessionTokenRest();
        response.setToken(token);
        return response;
    }

    @GetMapping("/{token}")
    @Parameter(in = ParameterIn.HEADER, description = "Session Token", name = "session_token", required = true, schema = @Schema(type = "string"))
    public SessionTokenRest checkSession(@PathVariable String token){
        sessionService.renewToken(token);
        SessionTokenRest response = new SessionTokenRest();
        response.setToken(token);
        return response;
    }
}
