package cz.tmsoft.ryby.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;

import cz.tmsoft.ryby.api.ClosePermitRest;
import cz.tmsoft.ryby.api.PermitRest;
import cz.tmsoft.ryby.domain.AttendaceClose;
import cz.tmsoft.ryby.mapper.PermitMapper;
import cz.tmsoft.ryby.service.PermitSevice;

import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @author tomas.marianek
 */
@RestController
@RequestMapping("/api/v1/permit")
public class PermitController {

    private PermitSevice permitService;
    
    private PermitMapper permitMapper;

    public PermitController(PermitSevice permitService, PermitMapper permitMapper) {
        this.permitService = permitService;
        this.permitMapper = permitMapper;
    }

    @GetMapping
    @Parameter(in = ParameterIn.HEADER,
        description = "Session Token",
        name = "session_token",
        required = true,
        schema = @Schema(type = "string"))
    public List<PermitRest> getAllPermit(@RequestHeader("session_token") String sessionToken) {
        return permitMapper.map(permitService.getAllPermit(sessionToken));
    }

    @GetMapping("/{uuid}")
    @Parameter(in = ParameterIn.HEADER, description = "Session Token", name = "session_token", required = true, schema = @Schema(type = "string"))
    public PermitRest getPermit(@NotEmpty @PathVariable String uuid) {
        return permitMapper.map(permitService.getPermit(uuid));
    }

    @PostMapping
    @Parameter(in = ParameterIn.HEADER, description = "Session Token", name = "session_token", required = true, schema = @Schema(type = "string"))
    public void savePermit(@Valid @RequestBody PermitRest request, @RequestHeader("session_token") String sessionToken) {
        permitService.savePermit(permitMapper.map(request), sessionToken);
    }

    @DeleteMapping("/{uuid}")
    @Parameter(in = ParameterIn.HEADER, description = "Session Token", name = "session_token", required = true, schema = @Schema(type = "string"))
    public void deletePermit(@NotEmpty @PathVariable String uuid) {
        permitService.deletePermit(uuid);
    }

    @PutMapping
    @Parameter(in = ParameterIn.HEADER, description = "Session Token", name = "session_token", required = true, schema = @Schema(type = "string"))
    public void updatePermit(@Valid @RequestBody PermitRest permitRest) {
        permitService.updatePermit(permitMapper.map(permitRest));
    }

    @GetMapping("/{uuid}/attendance")
    @Parameter(in = ParameterIn.HEADER, description = "Session Token", name = "session_token", required = true, schema = @Schema(type = "string"))
    public ClosePermitRest getClosePermit(@PathVariable String uuid) {
        List<AttendaceClose> result = permitService.getClosePermit(uuid);

        ClosePermitRest response = new ClosePermitRest();
        response.setCloseAttendanceRests(permitMapper.mapCloseAttendance(result));
        return response;
    }
}
