package cz.tmsoft.ryby.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

import cz.tmsoft.ryby.api.CloseAttendanceRest;
import cz.tmsoft.ryby.api.PermitRest;
import cz.tmsoft.ryby.domain.AttendaceClose;
import cz.tmsoft.ryby.domain.Permit;

/**
 * @author tomas.marianek
 */
@Mapper
public interface PermitMapper {

    List<PermitRest> map(List<Permit> allPermit);

    PermitRest map(Permit permit);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "userId", ignore = true)
    Permit map(PermitRest request);

    List<CloseAttendanceRest> mapCloseAttendance(List<AttendaceClose> result);
}
