package cz.tmsoft.ryby.mapper;

import org.mapstruct.Mapper;

import cz.tmsoft.ryby.api.RegistrationRest;
import cz.tmsoft.ryby.domain.Registration;

/**
 * @author tomas.marianek
 */
@Mapper
public interface RegistrationMapper {

    Registration map(RegistrationRest registrationRest);
}
