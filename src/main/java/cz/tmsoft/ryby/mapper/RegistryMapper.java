package cz.tmsoft.ryby.mapper;

import org.mapstruct.Mapper;

import java.util.List;

import cz.tmsoft.ryby.api.RegistryRest;
import cz.tmsoft.ryby.domain.Registry;

/**
 * @author tomas.marianek
 */
@Mapper
public interface RegistryMapper {

    List<RegistryRest> map(List<Registry> registries);
}

