package cz.tmsoft.ryby.mapper;


import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import cz.tmsoft.ryby.api.UserRest;
import cz.tmsoft.ryby.domain.User;

/**
 * @author tomas.marianek
 */
@Mapper
public interface SessionMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "uuid", ignore = true)
    @Mapping(target = "email", ignore = true)
    User map(UserRest userRest);
}
