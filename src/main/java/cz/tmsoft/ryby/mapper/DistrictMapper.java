package cz.tmsoft.ryby.mapper;


import org.mapstruct.Mapper;

import java.util.List;

import cz.tmsoft.ryby.api.DistrictRest;
import cz.tmsoft.ryby.domain.District;

/**
 * @author tomas.marianek
 */
@Mapper
public interface DistrictMapper {

    List<DistrictRest> map(List<District> allDistrict);
}
