package cz.tmsoft.ryby.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

import cz.tmsoft.ryby.api.AttendanceRest;
import cz.tmsoft.ryby.domain.Attendance;

/**
 * @author tomas.marianek
 */
@Mapper
public interface AttendanceMapper {

    List<AttendanceRest> map(List<Attendance> attendanceByPermission);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "idPovolenka", ignore = true)
    Attendance map(AttendanceRest request);
}
