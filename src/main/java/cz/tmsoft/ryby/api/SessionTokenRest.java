package cz.tmsoft.ryby.api;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author tomas.marianek
 */
@Data
@NoArgsConstructor
public class SessionTokenRest implements Serializable {

    private static final long serialVersionUID = -4280547973536278832L;
    private String token;
}
