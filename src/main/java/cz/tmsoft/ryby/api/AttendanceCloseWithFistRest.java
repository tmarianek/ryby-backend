package cz.tmsoft.ryby.api;

import java.math.BigDecimal;

import lombok.Data;

/**
 * @author tomas.marianek Since 23.11.2022
 */
@Data
public class AttendanceCloseWithFistRest {
    private String name;
    private String code;
    private Long pocet;
    private BigDecimal sum;

}
