package cz.tmsoft.ryby.api;

import lombok.Data;

/**
 * @author tomas.marianek
 */
@Data
public class RegistryRest {
    private String code;
    private String name;
}
