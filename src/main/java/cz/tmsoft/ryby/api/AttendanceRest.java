package cz.tmsoft.ryby.api;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * @author tomas.marianek
 */
@Data
public class AttendanceRest {
    @Schema(description = "Unique identification of Attendance", example = "f7a77c68-1e26-48c4-bd67-063a8ee7e222")
    private String uuid;

    private LocalDate datum;
    private String revir;
    private String podrevir;
    private String druh;
    private String mnozstvi;
    private BigDecimal vaha;
    private String delka;
    private String revirName;
    private String podrevirName;
}
