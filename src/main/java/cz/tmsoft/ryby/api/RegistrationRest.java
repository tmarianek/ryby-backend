package cz.tmsoft.ryby.api;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author tomas.marianek
 */
@Data
public class RegistrationRest implements Serializable {
    private static final long serialVersionUID = 6980121831728363168L;
    @Schema(description = "name", example = "name", required = true)
    @NotEmpty
    private String name;
    @Schema(description = "email", example = "email@email.com", required = true)
    @NotEmpty
    private String email;
    @Schema(description = "password", example = "pass", required = true)
    @NotEmpty
    private String password;
}
