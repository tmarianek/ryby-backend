package cz.tmsoft.ryby.api;

import lombok.Data;

/**
 * @author tomas.marianek
 */
@Data
public class DistrictRest {
    public String number;
    public String name;
}
