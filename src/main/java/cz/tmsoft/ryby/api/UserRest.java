package cz.tmsoft.ryby.api;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import lombok.*;


import java.io.Serializable;

/**
 * @author tomas.marianek
 */
@Data
public class UserRest implements Serializable {
    private static final long serialVersionUID = 3669722832288242999L;
    @Schema(description = "User name", example = "t.marianek@gmail.com", required = true)
    @NotEmpty
    private String name;
    @Schema(description = "user password", example = "pass", required = true)
    @NotEmpty
    private String pass;
}
