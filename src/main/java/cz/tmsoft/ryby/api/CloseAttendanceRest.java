package cz.tmsoft.ryby.api;


import java.math.BigDecimal;

import lombok.Data;

/**
 * @author tomas.marianek Since 23.11.2022
 */
@Data
public class CloseAttendanceRest {

    private String revir;
    private String podrevir;
    private String name;
    private Long count;
    private Long sumAll;
    private BigDecimal sumAllWeight;

    private AttendanceCloseWithFistRest candat;
    private AttendanceCloseWithFistRest amur;
    private AttendanceCloseWithFistRest bolen;
    private AttendanceCloseWithFistRest cejn;
    private AttendanceCloseWithFistRest hlavatka;
    private AttendanceCloseWithFistRest jelecJesen;
    private AttendanceCloseWithFistRest tloust;
    private AttendanceCloseWithFistRest kapr;
    private AttendanceCloseWithFistRest karas;
    private AttendanceCloseWithFistRest lipan;
    private AttendanceCloseWithFistRest lin;
    private AttendanceCloseWithFistRest mnik;
    private AttendanceCloseWithFistRest okoun;
    private AttendanceCloseWithFistRest ostroretka;
    private AttendanceCloseWithFistRest parma;
    private AttendanceCloseWithFistRest podoustev;
    private AttendanceCloseWithFistRest pstruhDuhovy;
    private AttendanceCloseWithFistRest pstruhObecny;
    private AttendanceCloseWithFistRest siven;
    private AttendanceCloseWithFistRest sumec;
    private AttendanceCloseWithFistRest marenaPeled;
    private AttendanceCloseWithFistRest stika;
    private AttendanceCloseWithFistRest tolstolobik;
    private AttendanceCloseWithFistRest uhor;
    private AttendanceCloseWithFistRest ostatni;
}
