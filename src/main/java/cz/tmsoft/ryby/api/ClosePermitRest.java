package cz.tmsoft.ryby.api;

import java.util.List;

import lombok.Data;

/**
 * @author tomas.marianek Since 23.11.2022
 */
@Data
public class ClosePermitRest {
    private List<CloseAttendanceRest> closeAttendanceRests;
}
