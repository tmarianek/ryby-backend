package cz.tmsoft.ryby.repository;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

import cz.tmsoft.ryby.domain.AttendaceClose;
import cz.tmsoft.ryby.domain.Attendance;
import cz.tmsoft.ryby.domain.AttendanceCloseWithFist;

/**
 * @author tomas.marianek
 */
@Mapper
public interface AttendanceRepository {
    @Select("""
            select d.UUID,
                   d.DATUM,
                   d.REVIR,
                   d.PODREVIR,
                   r.NAME as REVIRNAME,
                   pr.NAME as PODREVIRNAME,
                   reg.NAME as druh,
                   d.MNOZSTVI,
                   d.VAHA,
                   d.DELKA
            from RYBY.DOCHAZKA d
            join ryby.POVOLENKA p on p.ID = d.ID_POVOLENKA
            left join RYBY.REVIR r on r.CISLO = d.REVIR
            left join RYBY.PODREVIR pr on pr.CISLO = d.PODREVIR
            left join REGISTRY.VI_REGISTRY reg on reg.CODE = d.DRUH and REg.REG_CODE = '00006'
        where p.UUID = #{uuid}
                    """)
    List<Attendance> getAttendanceByPermission(String uuid);


    @Insert("""
        insert into ryby.DOCHAZKA (ID_POVOLENKA, UUID, DATUM, REVIR, PODREVIR, DRUH, MNOZSTVI, VAHA, DELKA)
        values ( #{idPovolenka}, #{uuid}, #{datum}, #{revir}, #{podrevir}, #{druh}, #{mnozstvi}, #{vaha}, #{delka} )
        """)
    void saveAttendance(Attendance attendance);

    @Select({"<script>", """
        select d.revir, d.PODREVIR, r.name, count(d.id) as count
        from RYBY.DOCHAZKA d
        join ryby.POVOLENKA p on p.ID = d.ID_POVOLENKA
        join ryby.REVIR r on r.CISLO = d.REVIR
        where p.UUID = #{uuid}
        group by d.revir, d.PODREVIR
        order by d.revir, d.PODREVIR """,
             "</script>"
    })
    List<AttendaceClose> getCloseAttendance(String uuid);

    @Select({"<script>", """
        select count(d.id) as pocet, sum(cast(d.vaha as FLOAT)) as sum
        from RYBY.DOCHAZKA d
                 join ryby.POVOLENKA p on p.ID = d.ID_POVOLENKA
        where p.UUID = #{uuid}
          and d.REVIR = #{revir} """,
             "<if test=\"podrevir != null\">",
             "and d.podrevir = #{podrevir}",
             "</if>",
             "and d.DRUH IN",
             "<foreach item='item' index='index' collection='permitFish' open='(' separator=',' close=')'>",
             "#{item}",
             "</foreach>",
             """
                 group by d.revir, d.podrevir
                 order by d.revir, d.podrevir
                 """,
             "</script>"})
    AttendanceCloseWithFist getCloseAttendanceWithFish(String uuid, String revir, String podrevir, List<String> permitFish);
}
