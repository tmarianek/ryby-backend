package cz.tmsoft.ryby.repository;

import cz.tmsoft.ryby.domain.Registry;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author tomas.marianek
 */
@Mapper
public interface RegistryRepository {

    @Select("""
            select CODE, name from REGISTRY.VI_REGISTRY
            where REG_CODE = #{code}
            """)
    List<Registry> getRegistryByCode(String code);
}
