package cz.tmsoft.ryby.repository;

import cz.tmsoft.ryby.domain.District;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author tomas.marianek
 */
@Mapper
public interface DistrictRepository {

    @Select("select cislo as number, name from RYBY.REVIR")
    List<District> getAllDistrict();

    @Select("""
            select p.CISLO as number, p.NAME from RYBY.PODREVIR p\s
            join ryby.REVIR R on p.ID_REVIR = R.ID
            where r.CISLO = #{number}
            """)
    List<District> getAllDistrictByDistrict(String number);
}
