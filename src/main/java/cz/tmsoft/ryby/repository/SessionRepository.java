package cz.tmsoft.ryby.repository;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.time.LocalDateTime;

import cz.tmsoft.ryby.domain.Pass;
import cz.tmsoft.ryby.domain.Session;
import cz.tmsoft.ryby.domain.User;

/**
 * @author tomas.marianek
 */
@Mapper
public interface SessionRepository {

    @Select("""
            select id, id_users, session_uuid, valid_from, valid_to
            from users.SESSION
            where SESSION_UUID = #{sessionToken}
            and #{sysDate} between valid_from and valid_to
            """)
    Session getSessionToken(String sessionToken, LocalDateTime sysDate);

    @Update("UPDATE USERS.SESSION set VALID_TO = #{validTo} where SESSION_UUID = #{sessionUuid}")
    void updateSessionTokenValidTo(Session session);

    @Delete("delete from  users.SESSION where SESSION_UUID = #{sessionUuid}")
    void removeSessionToken(String sessionUuid);

    @Select("select id, uuid, email, name from users.USERS where NAME = #{name}")
    User getUsersByUseName(String name);

    @Delete("delete from USERS.SESSION where ID_USERS = #{idUser}")
    void deleteAllSessionForUser(Long idUser);

    @Insert("""
        insert into USERS.SESSION (id_users, session_uuid, valid_from, valid_to)
        values ( #{idUsers}, #{sessionUuid}, #{validFrom}, #{validTo} )
        """)
    void createNewSession(Session session);

    @Select("""
        select ID, ID_USER, PASS, VALID_FROM, VALID_TO from USERS.PASS 
        where ID_USER = #{idUser} 
        and #{sysdate} between VALID_FROM and VALID_TO""")
    Pass getUserPassword(Long idUser, LocalDateTime sysdate);

    @Select("select id, uuid, email, name from users.USERS where email = #{name}")
    User getUsersByEmail(String name);

    @Select("select ID_USERS from users.session where session_uuid = #{sessionToken}")
    Long getUserIdForSession(String sessionToken);
}
