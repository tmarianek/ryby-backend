package cz.tmsoft.ryby.repository;

import cz.tmsoft.ryby.domain.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

/**
 * @author tomas.marianek
 */
@Mapper
public interface UserRepository {

    @Insert("insert into USERS.USERS (UUID, EMAIL, NAME) " +
            "values ( #{uuid}, #{email}, #{name} )")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void createUser(User user);

    @Select("select ID, UUID, EMAIL, NAME from USERS.USERS where EMAIL = #{email}")
    User getUserByEmail(String email);
}
