package cz.tmsoft.ryby.repository;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

import cz.tmsoft.ryby.domain.Permit;

/**
 * @author tomas.marianek
 */
@Mapper
public interface PermitRepository {


    @Select("""
            SELECT p.id, 
                 p.ID_USER as userId,
                 p."UUID",
                 p.NAME,
                 vr.NAME             AS svaz,
                 vr2.NAME            AS druh,
                 vr3.NAME            AS kategorie,
                 vr4.NAME            AS typ,
                 p.MISTNI_ORGANIZACE as mistniOrganizace,
                 p.PLATNOST_OD       as platnostOd,
                 p.PLATNOST_DO       as platnostDo,
                 vr5.NAME            AS stav
             FROM RYBY.POVOLENKA p
             JOIN USERS.SESSION s ON s.ID_USERS = p.ID_USER
             left JOIN REGISTRY.VI_REGISTRY vr ON vr.CODE = p.SVAZ AND vr.REG_CODE = '00001'
             left JOIN REGISTRY.VI_REGISTRY vr2 ON vr2.CODE = p.DRUH AND vr2.REG_CODE = '00002'
             left JOIN REGISTRY.VI_REGISTRY vr3 ON vr3.CODE = p.KATEGORIE AND vr3.REG_CODE = '00003'
             left JOIN REGISTRY.VI_REGISTRY vr4 ON vr4.CODE = p.TYP AND vr4.REG_CODE = '00004'
             left JOIN REGISTRY.VI_REGISTRY vr5 ON vr5.CODE = p.STAV AND vr5.REG_CODE = '00005'
             WHERE s.SESSION_UUID = #{sessionToken}                    
                         """)
    List<Permit> getAllPermit(String sessionToken);

    @Select("""
        SELECT p.id, 
            p.ID_USER as userId,
            p."UUID",
            p.NAME,
            vr.NAME             AS svaz,
            p.SVAZ              as svazCode,
            vr2.NAME            AS druh,
            p.DRUH              as druhCode,
            vr3.NAME            AS kategorie,
            p.KATEGORIE         as kategorieCode,
            vr4.NAME            AS typ,
            p.TYP               as typCode,
            p.MISTNI_ORGANIZACE as mistniOrganizace,
            p.PLATNOST_OD       as platnostOd,
            p.PLATNOST_DO       as platnostDo,
            vr5.NAME            AS stav,
            p.STAV              as stavCode  
        FROM RYBY.POVOLENKA p
        JOIN USERS.SESSION s ON s.ID_USERS = p.ID_USER
        left JOIN REGISTRY.VI_REGISTRY vr ON vr.CODE = p.SVAZ AND vr.REG_CODE = '00001'
        left JOIN REGISTRY.VI_REGISTRY vr2 ON vr2.CODE = p.DRUH AND vr2.REG_CODE = '00002'
            left JOIN REGISTRY.VI_REGISTRY vr3 ON vr3.CODE = p.KATEGORIE AND vr3.REG_CODE = '00003'
            left JOIN REGISTRY.VI_REGISTRY vr4 ON vr4.CODE = p.TYP AND vr4.REG_CODE = '00004'
            left JOIN REGISTRY.VI_REGISTRY vr5 ON vr5.CODE = p.STAV AND vr5.REG_CODE = '00005'
            WHERE p.uuid = #{uuid} 
            """)
    Permit getPermit(String uuid);

    @Insert("""
        INSERT INTO RYBY.POVOLENKA
        (ID_USER, "UUID", NAME, SVAZ, DRUH, KATEGORIE, TYP, MISTNI_ORGANIZACE, PLATNOST_OD, PLATNOST_DO, STAV)
        VALUES( #{userId},#{uuid}, #{name}, #{svaz}, #{druh}, #{kategorie}, #{typ}, #{mistniOrganizace}, #{platnostOd}, #{platnostDo}, #{stav});                                                                            
                   """)
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void savePermit(Permit permit);

    @Delete("Delete from ryby.povolenka where uuid = #{uuid}")
    void deletePermit(String uuid);

    @Update("""
        update ryby.POVOLENKA
        set  NAME = #{name},
             SVAZ = #{svazCode},
             DRUH = #{druhCode},
             KATEGORIE = #{kategorieCode},
             TYP = #{typCode},
             MISTNI_ORGANIZACE = #{mistniOrganizace},
             PLATNOST_OD = #{platnostOd},
             PLATNOST_DO = #{platnostDo},
             STAV = #{stavCode}
        where uuid = #{uuid}       
        """)
    void updatePermition(Permit permit);
}
