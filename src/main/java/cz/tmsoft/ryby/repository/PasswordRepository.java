package cz.tmsoft.ryby.repository;

import cz.tmsoft.ryby.domain.Pass;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;

/**
 * @author tomas.marianek
 */
@Mapper
public interface PasswordRepository {
    @Insert("insert into USERS.PASS (ID_USER, PASS, VALID_FROM, VALID_TO) " +
            "values ( #{idUser}, #{pass}, #{validFrom}, #{validTo} )")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void createPassword(Pass pass);
}
