package cz.tmsoft.ryby.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;

import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tomas.marianek
 */
@Configuration
@OpenAPIDefinition
public class OpenApiConfig {

    @Bean
    public OpenAPI rybyOpenAPI() {
        return new OpenAPI()
                .info(
                        new Info()
                                .title("Ryby API")
                                .description("Ryby application")
                                .version("0.0.1")
                                .contact(new Contact().name("Tomas Marianek").email("tomas.marianek@gmail.com"))
                );
    }

    @Bean
    public GroupedOpenApi rybyGroupApi() {
        return GroupedOpenApi.builder()
                .group("ryby")
                .pathsToMatch("/api/v1/**")
                .packagesToScan("cz.tmsoft.ryby.controller")
                .build();
    }
}
