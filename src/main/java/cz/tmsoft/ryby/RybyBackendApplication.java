package cz.tmsoft.ryby;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableEncryptableProperties
public class RybyBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(RybyBackendApplication.class, args);
    }

}
