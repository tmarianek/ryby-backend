package cz.tmsoft.ryby.errors;

import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

import lombok.Builder;
import lombok.Data;

/**
 * @author tomas.marianek
 */
@Data
@Builder
public class ErrorInfo {
    private String errorCode;
    private HttpStatus httpStatus;
    private String url;
    private LocalDateTime timeStamp;
    private String description;
}
