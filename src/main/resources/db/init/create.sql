--liquibase formatted sql
--changeset t.marianek@gmail.com:create-tables
--comment: create Ryby table
--
create schema if not exists users;

create table if not exists users.users
(
    id    bigint primary key auto_increment,
    uuid  varchar(36),
    email varchar(100),
    name  varchar(50)
);

create table if not exists users.pass
(
    id         bigint primary key auto_increment,
    id_user    bigint,
    pass       varchar(30),
    valid_from date,
    valid_to   date,
    foreign key (id_user) references users.users (id)
);

create table if not exists users.session
(
    id           bigint primary key auto_increment,
    id_users     bigint,
    session_uuid varchar(36),
    valid_from   timestamp,
    valid_to     timestamp,
    foreign key (id_users) references users.users (id)
);

create schema if not exists ryby;

create table if not exists ryby.povolenka
(
    id          bigint primary key auto_increment,
    id_user     bigint,
    uuid        varchar(36),
    name        varchar(50),
    svaz        varchar(5),
    druh        varchar(5),
    kategorie   varchar(5),
    typ         varchar(5),
    mistni_organizace varchar(50),
    platnost_od date,
    platnost_do date,
    stav        varchar(5),
    foreign key (id_user) references users.users (id)
);

create table if not exists ryby.dochazka
(
    id           bigint primary key auto_increment,
    id_povolenka bigint,
    uuid         varchar(36),
    datum        date,
    revir        varchar(6),
    podrevir     varchar(6),
    druh         varchar(5),
    mnozstvi     varchar(5),
    vaha         varchar(5),
    delka        varchar(5),
    foreign key (id_povolenka) references ryby.povolenka (ID)
);

create schema if not exists registry;

create table if not exists registry.registry
(
    id   bigint primary key auto_increment,
    code varchar(5) unique,
    name varchar(50)
);

create table if not exists registry.registry_item
(
    id          bigint primary key auto_increment,
    id_registry bigint,
    code        varchar(5),
    name        varchar(50),
    foreign key (id_registry) references registry.registry (id)
);



create table if not exists ryby.revir
(
    id         bigint primary key auto_increment,
    cislo      varchar(6),
    name       varchar(100),
    typ_reviru varchar(5)
);

create table if not exists ryby.podrevir
(
    id       bigint primary key auto_increment,
    id_revir bigint,
    cislo    varchar(6),
    name     varchar(100),
    foreign key (id_revir) references ryby.revir (id)
);


create view  if not exists  registry.vi_registry as
select ri.CODE, ri.NAME, r.NAME as reg_name, r.code as reg_code, r.ID as reg_id
from REGISTRY.REGISTRY r
join REGISTRY.REGISTRY_ITEM RI
     on r.ID = RI.ID_REGISTRY;

