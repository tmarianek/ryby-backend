--liquibase formatted sql
--changeset t.marianek@gmail.com:initData
--comment: init data  Ryby

insert into REGISTRY.REGISTRY (code, NAME)
values ('00001', 'Svazy');

insert into REGISTRY.REGISTRY_ITEM (ID_REGISTRY, CODE, NAME)
values ((select id from REGISTRY.REGISTRY where name = 'Svazy'), 'sc', 'Stredocesky uzemni svaz');
insert into REGISTRY.REGISTRY_ITEM (ID_REGISTRY, CODE, NAME)
values ((select id from REGISTRY.REGISTRY where name = 'Svazy'), 'mp', 'Uzemni svaz mesta Prahy');
insert into REGISTRY.REGISTRY_ITEM (ID_REGISTRY, CODE, NAME)
values ((select id from REGISTRY.REGISTRY where name = 'Svazy'), 'jc', 'Jihocesky uzemni svaz');
insert into REGISTRY.REGISTRY_ITEM (ID_REGISTRY, CODE, NAME)
values ((select id from REGISTRY.REGISTRY where name = 'Svazy'), 'zc', 'Zapadocesky uzemni svaz');
insert into REGISTRY.REGISTRY_ITEM (ID_REGISTRY, CODE, NAME)
values ((select id from REGISTRY.REGISTRY where name = 'Svazy'), 'sevc', 'Severocesky uzemni svaz');
insert into REGISTRY.REGISTRY_ITEM (ID_REGISTRY, CODE, NAME)
values ((select id from REGISTRY.REGISTRY where name = 'Svazy'), 'vc', 'Vychodocesky uzemni svaz');
insert into REGISTRY.REGISTRY_ITEM (ID_REGISTRY, CODE, NAME)
values ((select id from REGISTRY.REGISTRY where name = 'Svazy'), 'sms', 'Uzemni svaz pro Severni moravu a Slezko');
insert into REGISTRY.REGISTRY_ITEM (ID_REGISTRY, CODE, NAME)
values ((select id from REGISTRY.REGISTRY where name = 'Svazy'), 'mrs', 'Moravský rybářský svaz');
insert into REGISTRY.REGISTRY_ITEM (ID_REGISTRY, CODE, NAME)
values ((select id from REGISTRY.REGISTRY where name = 'Svazy'), 'cel', 'Celosvazové');
insert into REGISTRY.REGISTRY_ITEM (ID_REGISTRY, CODE, NAME)
values ((select id from REGISTRY.REGISTRY where name = 'Svazy'), 'cr', 'Celorepublikové');


insert into REGISTRY.REGISTRY (code, name)
values ('00002', 'Druh reviru');
insert into REGISTRY.REGISTRY_ITEM (ID_REGISTRY, CODE, NAME)
values ((select id from REGISTRY.REGISTRY where name = 'Druh reviru'), 'mr', 'Mimopstruhovy revir');
insert into REGISTRY.REGISTRY_ITEM (ID_REGISTRY, CODE, NAME)
values ((select id from REGISTRY.REGISTRY where name = 'Druh reviru'), 'pr', 'Ostruhovy revir');


insert into REGISTRY.REGISTRY (code, name)
values ('00003', 'Kategorie');
insert into REGISTRY.REGISTRY_ITEM (ID_REGISTRY, CODE, NAME)
values ((select id from REGISTRY.REGISTRY where name = 'Kategorie'), 'dos', 'Dospeli');
insert into REGISTRY.REGISTRY_ITEM (ID_REGISTRY, CODE, NAME)
values ((select id from REGISTRY.REGISTRY where name = 'Kategorie'), 'mld', 'Mladez');
insert into REGISTRY.REGISTRY_ITEM (ID_REGISTRY, CODE, NAME)
values ((select id from REGISTRY.REGISTRY where name = 'Kategorie'), 'det', 'Deti');
insert into REGISTRY.REGISTRY_ITEM (ID_REGISTRY, CODE, NAME)
values ((select id from REGISTRY.REGISTRY where name = 'Kategorie'), 'ztp', 'ZTP/P');

insert into REGISTRY.REGISTRY (code, name)
values ('00004', 'Typ povolenky');
insert into REGISTRY.REGISTRY_ITEM (ID_REGISTRY, CODE, NAME)
values ((select id from REGISTRY.REGISTRY where name = 'Typ povolenky'), '1', '1 denni');
insert into REGISTRY.REGISTRY_ITEM (ID_REGISTRY, CODE, NAME)
values ((select id from REGISTRY.REGISTRY where name = 'Typ povolenky'), '2', '2 denni');
insert into REGISTRY.REGISTRY_ITEM (ID_REGISTRY, CODE, NAME)
values ((select id from REGISTRY.REGISTRY where name = 'Typ povolenky'), '3', '3 denni');
insert into REGISTRY.REGISTRY_ITEM (ID_REGISTRY, CODE, NAME)
values ((select id from REGISTRY.REGISTRY where name = 'Typ povolenky'), '7', '7 denni');
insert into REGISTRY.REGISTRY_ITEM (ID_REGISTRY, CODE, NAME)
values ((select id from REGISTRY.REGISTRY where name = 'Typ povolenky'), '10', '10 denni');
insert into REGISTRY.REGISTRY_ITEM (ID_REGISTRY, CODE, NAME)
values ((select id from REGISTRY.REGISTRY where name = 'Typ povolenky'), '14', '14 denni');
insert into REGISTRY.REGISTRY_ITEM (ID_REGISTRY, CODE, NAME)
values ((select id from REGISTRY.REGISTRY where name = 'Typ povolenky'), '17', '17 denni');
insert into REGISTRY.REGISTRY_ITEM (ID_REGISTRY, CODE, NAME)
values ((select id from REGISTRY.REGISTRY where name = 'Typ povolenky'), 'm', 'mesicni');
insert into REGISTRY.REGISTRY_ITEM (ID_REGISTRY, CODE, NAME)
values ((select id from REGISTRY.REGISTRY where name = 'Typ povolenky'), 'r', 'rocni');

insert into REGISTRY.REGISTRY (code, name)
values ('00005', 'stav povolenky');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name)
values ((select id from REGISTRY.REGISTRY where name = 'stav povolenky'), 'a', 'Aktivni');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name)
values ((select id from REGISTRY.REGISTRY where name = 'stav povolenky'), 'u', 'Ukoncena');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name)
values ((select id from REGISTRY.REGISTRY where name = 'stav povolenky'), 'z', 'Zrusena');

insert into REGISTRY.REGISTRY (code, NAME)
values ('00006', 'druhy ryb');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'a', 'Amur bílý');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'b', 'Bolen dravý');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'c', 'Candát obecný');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'd', 'Candát východní');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'e', 'Cejn perleťový');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'f', 'Cejn siný');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'g', 'Cejn velký');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'h', 'Cejnek malý');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'j', 'Drsek menší');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'k', 'Drsek větší');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'l', 'Hlavatka obecná');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'm', 'Hlaváč černoústý');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'n', 'Hlavačka mramorovaná');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'o', 'Hořavka duhová');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'p', 'Hrouzek Kesslerův');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'q', 'Hrouzek běloploutvý');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'r', 'Hrouzek obecný');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 's', 'Jelec jesen');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 't', 'Jelec proudník');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'u', 'Jelec tloušť');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'v', 'Jeseter hvězdnatý');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'w', 'Jeseter malý');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'x', 'Jeseter ruský');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'y', 'Jeseter sibiřský');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'z', 'Jeseter velký');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'aa', 'Ježdík dunajský');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'ab', 'Ježdík obecný');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'ac', 'Ježdík žlutý');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'ad', 'Kapr obecný');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'ae', 'Karas obecný');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'af', 'Karas stříbřitý');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'ag', 'Koljuška tříostná');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'ah', 'Lipan podhorní');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'ai', 'Lín obecný');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'aj', 'Losos obecný');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'ak', 'Mník jednovousý');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'al', 'Mřenka mramorovaná');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'am', 'Okoun říční');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'an', 'Okounek pstruhový');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'ao', 'Ostroretka stěhovavá');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'ap', 'Ostrucha křivočará');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'aq', 'Ouklej obecná');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'ar', 'Ouklejka pruhovaná');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'as', 'Parma obecná');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'at', 'Perlín ostrobřichý');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'au', 'Piskoř pruhovaný');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'av', 'Plotice lesklá');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'aw', 'Plotice obecná');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'ax', 'Podoustev říční');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'ay', 'Pstruh duhový');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'az', 'Pstruh obecný');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'ba', 'Sekavčík horský');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'bb', 'Sekavec podunajský');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'bc', 'Siven americký');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'bd', 'Slunečnice pestrá');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'be', 'Slunka obecná');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'bf', 'Střevle potoční');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'bg', 'Střevlička východní');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'bh', 'Sumec velký');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'bi', 'Síh maréna');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'bj', 'Síh peleď');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'bk', 'Sumeček americký');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'bl', 'Sumeček tečkovaný');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'bm', 'Štika obecná');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'bn', 'Tolstolobec pestrý');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'bo', 'Tolstolobik bílý');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'bp', 'Vranka obecná');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'bq', 'Vranka pruhoploutvá');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'br', 'Vyza velká');
insert into REGISTRY.REGISTRY_ITEM (id_registry, code, name) values ((select id from REGISTRY.REGISTRY where name = 'druhy ryb'), 'bs', 'Úhoř říční');

insert into RYBY.REVIR (CISLO, NAME, TYP_REVIRU)
values ('471064', 'MO Bohumín', 'mr');

insert into RYBY.REVIR (CISLO, NAME, TYP_REVIRU)
values ('471068', 'Odra 3 A', 'mr');
insert into RYBY.PODREVIR (ID_REVIR, CISLO, NAME)
values ((select id from RYBY.REVIR where REVIR.CISLO = '471068'), '1', 'rameno u VŽ');
insert into RYBY.PODREVIR (ID_REVIR, CISLO, NAME)
values ((select id from RYBY.REVIR where REVIR.CISLO = '471068'), '2', 'Výškovické tůně');
insert into RYBY.PODREVIR (ID_REVIR, CISLO, NAME)
values ((select id from RYBY.REVIR where REVIR.CISLO = '471068'), '3', 'Polanecká tůň 1 – 3');


-- insert tech user
insert into users.USERS (UUID, EMAIL, NAME)
values ('1', 't.marianek@gmail.com', 'tmarianek');

insert into USERS.PASS (ID_USER, PASS, VALID_FROM, VALID_TO)
values ((select id from USERS.USERS where EMAIL = 't.marianek@gmail.com'), 'pass', current_timestamp(),
        DATEADD('DAY', 7, CURRENT_TIMESTAMP()));

insert into RYBY.POVOLENKA (NAME, ID_USER, UUID, SVAZ, DRUH, KATEGORIE, TYP, PLATNOST_OD, PLATNOST_DO, STAV)
values ('roci 2022', (select id from USERS.USERS where EMAIL = 't.marianek@gmail.com'), RANDOM_UUID(), 'sms', 'mr',
        'dos', 'r', parsedatetime('01-01-2022', 'dd-MM-yyyy'),
        parsedatetime('31-12-2022', 'dd-MM-yyyy'), 'a');

insert into RYBY.DOCHAZKA (ID_POVOLENKA, UUID, DATUM, REVIR, PODREVIR, DRUH, MNOZSTVI, VAHA, DELKA)
values ((select id from RYBY.POVOLENKA where NAME = 'roci 2022'), '1a40128e-b4b2-11ec-b909-0242ac120002',
        parsedatetime('14-03-2022', 'dd-MM-yyyy'),
        '471064', null, null, null, null, null);
insert into RYBY.DOCHAZKA (ID_POVOLENKA, UUID, DATUM, REVIR, PODREVIR, DRUH, MNOZSTVI, VAHA, DELKA)
values ((select id from RYBY.POVOLENKA where NAME = 'roci 2022'), '350b7630-b4b2-11ec-b909-0242ac120002',
        parsedatetime('15-03-2022', 'dd-MM-yyyy'),
        '471064', null, 'ad', '1', '4.25', '54');

