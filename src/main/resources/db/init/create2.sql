--liquibase formatted sql
--changeset t.marianek@gmail.com:create-tables
--comment: create view dochazka count
--

create view if not exists RYBY.vi_dochazka_count as
select d.revir,
       count(d.id) as pocet
from RYBY.DOCHAZKA d
group by d.revir
order by d.revir;
