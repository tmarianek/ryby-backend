package cz.tmsoft.ryby.repository;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import cz.tmsoft.ryby.domain.AttendaceClose;
import cz.tmsoft.ryby.domain.AttendanceCloseWithFist;
import cz.tmsoft.ryby.domain.enums.PermitFish;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author tomas.marianek Since 30.09.2022
 */
@SpringBootTest
@Tag("integration")
class AttendanceRepositoryIntegrationTest {

    @Autowired
    AttendanceRepository attendanceRepository;

    @Test
    void getCloseAttendance() {
        List<AttendaceClose> closeAttendance = attendanceRepository.getCloseAttendance("28d32969-b020-4584-b13b-6dbacb166876");
        assertNotNull(closeAttendance);
        assertTrue(closeAttendance.size() > 0);
    }

    //@Test
//    void getCloseAttendanceWithFish() {
//        String uuid = "91c6ac50-0a22-4b3d-9fce-46e338592120";
//        List<AttendaceClose> closeAttendance = attendanceRepository.getCloseAttendance(uuid);
//
//        for (AttendaceClose close : closeAttendance) {
//            close.setKapr(
//                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.KAPR_OBECNY.getValue())));
//            close.setLin(
//                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.LIN_OBECNY.getValue())));
//            close.setCejn(
//                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), PermitFish.getCejn()));
//            close.setAmur(
//                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.AMUR_BILY.getValue())));
//            close.setCandat(attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), PermitFish.getCandat()));
//            close.setBolen(
//                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.BOLEN_DRAVY.getValue())));
//            close.setHlavatka(
//                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.HLAVATKA_OBECNA.getValue())));
//            close.setKaras(attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), PermitFish.getKaras()));
//            close.setLipan(
//                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.LIPAN_PODHORNI.getValue())));
//            close.setMnik(
//                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.MNIK_JEDNOVOUSY.getValue())));
//            close.setParma(
//                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.PARMA_OBECNA.getValue())));
//            close.setJelecJesen(
//                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.JELEC_JESEN.getValue())));
//            close.setTloust(
//                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.JELEC_TLOUST.getValue())));
//            close.setOkoun(
//                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.OKOUN_RICNI.getValue())));
//            close.setOstroretka(attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(),
//                                                                                List.of(PermitFish.OSTRORETKA_STEHOVAVA.getValue())));
//            close.setPodoustev(
//                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.PODOUSTEV_RICNI.getValue())));
//            close.setPstruhDuhovy(
//                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.PSTRUH_DUHOVY.getValue())));
//            close.setPstruhObecny(
//                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.PSTRUH_OBECNY.getValue())));
//            close.setSiven(
//                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.SIVEN_AMERICKY.getValue())));
//            close.setMarenaPeled(attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), PermitFish.getSin()));
//            close.setStika(
//                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.STIKA_OBECNA.getValue())));
//            close.setSumec(
//                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.SUMEC_VELKY.getValue())));
//            close.setTolstolobik(
//                attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.TOLSTOLOBIK_BILY.getValue())));
//            close.setUhor(attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), List.of(PermitFish.UHOR_RICNI.getValue())));
//            close.setOstatni(attendanceRepository.getCloseAttendanceWithFish(uuid, close.getRevir(), close.getPodrevir(), PermitFish.getOstatni()));
//        }
//
//        System.out.println(closeAttendance);
//        assertNotNull(closeAttendance);
//        assertTrue(closeAttendance.size() > 0);
//        assertNotNull(closeAttendance.get(0).getKapr());
//    }
}
