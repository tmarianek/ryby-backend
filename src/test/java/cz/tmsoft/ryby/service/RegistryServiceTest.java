package cz.tmsoft.ryby.service;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import cz.tmsoft.ryby.domain.Registry;
import cz.tmsoft.ryby.repository.RegistryRepository;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {RegistryService.class})
@ExtendWith(SpringExtension.class)
class RegistryServiceTest {
    @MockBean
    private RegistryRepository registryRepository;

    @Autowired
    private RegistryService registryService;

    /**
     * Method under test: {@link RegistryService#getRegistryByCode(String)}
     */
    @Test
    void testGetRegistryByCode() {
        ArrayList<Registry> registryList = new ArrayList<>();
        when(registryRepository.getRegistryByCode((String) any())).thenReturn(registryList);
        List<Registry> actualRegistryByCode = registryService.getRegistryByCode("Code");
        assertSame(registryList, actualRegistryByCode);
        assertTrue(actualRegistryByCode.isEmpty());
        verify(registryRepository).getRegistryByCode((String) any());
    }
}

