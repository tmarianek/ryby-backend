package cz.tmsoft.ryby.integration;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import cz.tmsoft.ryby.api.AttendanceRest;
import cz.tmsoft.ryby.api.ClosePermitRest;
import cz.tmsoft.ryby.api.PermitRest;
import cz.tmsoft.ryby.api.RegistrationRest;
import cz.tmsoft.ryby.api.SessionTokenRest;
import cz.tmsoft.ryby.api.UserRest;
import cz.tmsoft.ryby.domain.Pass;
import cz.tmsoft.ryby.domain.enums.PermitFish;
import cz.tmsoft.ryby.integration.domain.User;
import cz.tmsoft.ryby.integration.utils.IntegrationTestUtils;
import cz.tmsoft.ryby.repository.SessionRepository;
import cz.tmsoft.ryby.repository.UserRepository;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author tomas.marianek
 */

public class CretePermissionIntegrationTest extends AbstractIntegrationTest {

    /**
     * scenar: 1. registrace uzivatele - done 2. prihlaseni uzivatele - node 3. zalozeni povolenky - done 4. zadani dochazky 5. uzavreni dochazky
     */

    @Autowired
    UserRepository userRepository;

    @Autowired
    SessionRepository sessionRepository;

    @Test
    public void create() {
        User user = createUser();
        registryUser(user);
        String token = loginUser(user);
        String permission = createpermission(token);
        createAttendance(token, permission);
        closepermission(token, permission);

        getCloseAttendance(token, permission);
    }

    private void getCloseAttendance(String token, String permission) {
        String url = IntegrationTestUtils.getCloseAttendance(port, permission);

        HttpEntity request = new HttpEntity(getHttpHeaders(token));

        ResponseEntity<ClosePermitRest> exchange = restTemplate.exchange(url, HttpMethod.GET, request, ClosePermitRest.class);
        assertNotNull(exchange);
        assertNotNull(exchange.getBody());

        ClosePermitRest body = exchange.getBody();

        assertNotNull(body.getCloseAttendanceRests());
        assertTrue(body.getCloseAttendanceRests().size() > 0);
    }

    private static HttpHeaders getHttpHeaders(String token) {
        HttpHeaders header = new HttpHeaders();
        header.add("session_token", token);
        return header;
    }

    private void closepermission(String token, String permission) {

        String url = IntegrationTestUtils.getPermitApi(port) + "/" + permission;
        String url1 = IntegrationTestUtils.getPermitApi(port);


        HttpEntity request = new HttpEntity<>(getHttpHeaders(token));

        ResponseEntity<PermitRest> exchange = restTemplate.exchange(url, HttpMethod.GET, request, PermitRest.class);

        assertNotNull(exchange);
        assertNotNull(exchange.getBody());

        PermitRest body = exchange.getBody();
        body.setStavCode("u");

        HttpEntity requestUpdate = new HttpEntity<>(body, getHttpHeaders(token));

        restTemplate.exchange(url1, HttpMethod.PUT, requestUpdate, Void.class);

        ResponseEntity<PermitRest> exchange1 = restTemplate.exchange(url, HttpMethod.GET, request, PermitRest.class);

        assertNotNull(exchange1);
        assertNotNull(exchange1.getBody());

        PermitRest body1 = exchange1.getBody();
        assertTrue("u".equals(body1.getStavCode()));
    }

    private void createAttendance(String token, String permission) {

        String url = IntegrationTestUtils.getAttendanceApi(port, permission);

        Long days = 0l;
        for(PermitFish permitFish :  PermitFish.values()) {
            AttendanceRest body = new AttendanceRest();
            LocalDate localDate = LocalDate.now().plusDays(days);
            body.setDatum(localDate);
            days++;
            body.setRevir("471064");
            //body.setPodrevir();
            body.setDruh(permitFish.getValue());
            body.setMnozstvi("1");
            body.setVaha(BigDecimal.valueOf(5));
            body.setDelka("50");

            HttpEntity request = new HttpEntity(body, getHttpHeaders(token));

            restTemplate.exchange(url, HttpMethod.POST, request, Void.class);

        }

        HttpEntity requestGet = new HttpEntity(getHttpHeaders(token));

        ResponseEntity<AttendanceRest[]> response = restTemplate.exchange(url, HttpMethod.GET, requestGet, AttendanceRest[].class);

        assertNotNull(response);
        assertNotNull(response.getBody());
        assertTrue(response.getBody().length > 0);
    }

    private String createpermission(String token) {

        PermitRest body = new PermitRest();
        body.setName("Celorovni 2022");
        body.setStav("a");
        body.setDruh("mr");
        body.setSvaz("sms");
        body.setKategorie("dos");
        body.setTyp("r");
        body.setMistniOrganizace("Ostrava");
        body.setPlatnostDo(LocalDate.of(2022, 1, 1));
        body.setPlatnostOd(LocalDate.of(2022, 12, 31));
        HttpEntity<PermitRest> request = new HttpEntity<>(body, getHttpHeaders(token));

        String url = IntegrationTestUtils.getPermitApi(port);
        restTemplate.exchange(url, HttpMethod.POST, request, Void.class);

        HttpEntity requstGet = new HttpEntity<>(getHttpHeaders(token));

        ResponseEntity<PermitRest[]> response = restTemplate.exchange(url, HttpMethod.GET, requstGet, PermitRest[].class);

        assertNotNull(response);
        assertNotNull(response.getBody());
        assertTrue(response.getBody().length > 0);

        PermitRest permitRest = response.getBody()[0];

        assertNotNull(permitRest.getUuid());

        return permitRest.getUuid();
    }

    private String loginUser(User user) {
        UserRest request = new UserRest();
        request.setName(user.getEmail());
        request.setPass(user.getPass());

        String url = IntegrationTestUtils.getLoginUrl(port);
        ResponseEntity<SessionTokenRest> responseEntity = restTemplate.postForEntity(url, request, SessionTokenRest.class);

        assertNotNull(responseEntity);
        assertNotNull(responseEntity.getBody());
        assertNotNull(responseEntity.getBody().getToken());

        return responseEntity.getBody().getToken();
    }

    private void registryUser(User user) {
        RegistrationRest request = new RegistrationRest();
        request.setEmail(user.getEmail());
        request.setPassword(user.getPass());
        request.setName(user.getName());

        String url = IntegrationTestUtils.getRegistrationApi(port);
        restTemplate.postForEntity(url, request, Void.class);

        cz.tmsoft.ryby.domain.User userByEmail = userRepository.getUserByEmail(user.getEmail());
        Pass userPassword = sessionRepository.getUserPassword(userByEmail.getId(), LocalDateTime.now());

        assertNotNull(userByEmail);
        assertTrue(userByEmail.getEmail().equals(user.getEmail()));
        assertTrue(userByEmail.getName().equals(user.getName()));
        assertTrue(userPassword.getPass().equals(user.getPass()));
    }

    private User createUser() {
        return new User("name", createEmail(), "pass");

    }

    private String createEmail() {
        String email = RandomStringUtils.random(12, true, false);
        email += "@";
        email += "gmail.com";
        return email;
    }

}
