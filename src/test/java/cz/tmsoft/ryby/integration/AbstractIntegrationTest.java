package cz.tmsoft.ryby.integration;

import org.junit.jupiter.api.Tag;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.web.client.RestTemplate;

/**
 * @author tomas.marianek Since 27.09.2022
 */

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Tag("integration")
public class AbstractIntegrationTest {
    @LocalServerPort
    protected int port;

    protected RestTemplate restTemplate = new RestTemplate();
}
