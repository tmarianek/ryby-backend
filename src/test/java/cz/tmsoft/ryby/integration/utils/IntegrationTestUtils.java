package cz.tmsoft.ryby.integration.utils;

/**
 * @author tomas.marianek Since 27.09.2022
 */
public class IntegrationTestUtils {

    private static final String SERVER = "http://localhost:";
    private static final String REGISTRATION_API = "/api/v1/registration";
    private static final String LOGU_API = "/api/v1/session/login";
    private static final String PERMIT_API = "/api/v1/permit";
    private static final String ATTENDANCE_API = "/api/v1/attendance";

    public static String getRegistrationApi(int port) {
        return SERVER + port + REGISTRATION_API;
    }

    public static String getLoginUrl(int port) {
        return SERVER + port + LOGU_API;
    }

    public static String getPermitApi(int port) {
        return SERVER + port + PERMIT_API;
    }

    public static String getAttendanceApi(int port, String permition) {
        return SERVER + port + ATTENDANCE_API + "/" + permition;
    }

    public static String getCloseAttendance(int port, String permition) {
        return SERVER + port + PERMIT_API + "/" + permition + "/attendance";
    }
}
