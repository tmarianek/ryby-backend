package cz.tmsoft.ryby.integration;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;

import cz.tmsoft.ryby.api.RegistrationRest;
import cz.tmsoft.ryby.domain.Pass;
import cz.tmsoft.ryby.domain.User;
import cz.tmsoft.ryby.integration.utils.IntegrationTestUtils;
import cz.tmsoft.ryby.repository.SessionRepository;
import cz.tmsoft.ryby.repository.UserRepository;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author tomas.marianek Since 08.09.2022
 */

public class RegistrationIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    UserRepository userRepository;

    @Autowired
    SessionRepository sessionRepository;


    @Test
    public void registration() {
        String email = createEmail();
        String name = "name";
        String pass = "pass";

        RegistrationRest request = new RegistrationRest();
        request.setEmail(email);
        request.setPassword(pass);
        request.setName(name);

        String url = IntegrationTestUtils.getRegistrationApi(port);
        restTemplate.postForEntity(url, request, Void.class);

        User userByEmail = userRepository.getUserByEmail(email);
        Pass userPassword = sessionRepository.getUserPassword(userByEmail.getId(), LocalDateTime.now());

        assertNotNull(userByEmail);
        assertTrue(userByEmail.getEmail().equals(email));
        assertTrue(userByEmail.getName().equals(name));
        assertTrue(userPassword.getPass().equals(pass));
    }

    private String createEmail() {
        String email = RandomStringUtils.random(12, true, false);
        email += "@";
        email += "gmail.com";
        return email;
    }
}
